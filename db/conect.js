const express = require('express')
const mysql = require('mysql');
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese_main',
    port: '3306',
}

const connection = mysql.createConnection(options);//实例化连接


// 服务器连接
connection.connect(function (err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('连接数据库成功。。。。connected as id ' + connection.threadId);
});