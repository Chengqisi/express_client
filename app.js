var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin')
var basketRouter = require('./routes/basket')
var goodRouter = require('./routes/good')
var comRouter = require('./routes/community')
var bookRouter = require('./routes/book/index')
var socreRouter = require('./routes/hospital/goods')
var bookStore = require('./routes/book/books')
var commentRouter = require('./routes/book/comment')
var libraryRouter = require('./routes/book/library')
var newsRouter = require('./routes/news')

var app = express();
require('./db/conect.js')
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/index', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', adminRouter)
app.use('/basket', basketRouter)
app.use('/good', goodRouter)
app.use('/com', comRouter)
app.use('/score', socreRouter)
app.use('/book', bookRouter)
app.use('/bookstore', bookStore)
app.use('/comment', commentRouter)
app.use('/library', libraryRouter)
app.use('/news', newsRouter)
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
