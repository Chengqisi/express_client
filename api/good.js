const mysql = require("mysql")
const { Sequelize } = require('sequelize');

const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const db = new Sequelize('cheese', 'cheese_main', 'cheese_main', {
    host: 'cheese.mysql.rds.aliyuncs.com',
    port: "3306",
    dialect: "mysql"
})
var goodMode=db.define('goods',{
    name   :Sequelize.STRING,
    tag    :Sequelize.STRING,
    msg    :Sequelize.STRING,
    url    :Sequelize.STRING,
    price  :Sequelize.STRING,
    imgurl :Sequelize.STRING
},{timestamps:false,freezeTableName:true})

const connect = mysql.createConnection(options)
const getgood = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from goods limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from goods"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

const add = (params) => {
    console.log(params, "=======>api");
    let { name, price, msg, url, imgurl, tag} = params
    const add_sql = `insert into goods(name, price, msg, url, imgurl, tag)   VALUES (?,?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [name, price, msg, url, imgurl, tag],function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true ,data:data})
            }
        })
    })
    return p
}
const del=async(params)=>{
    var res = await goodMode.destroy({
        where: {
            ...params
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
module.exports = {
    getgood,
    add,del
}