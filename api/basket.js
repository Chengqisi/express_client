const { param } = require("express/lib/request");
const mysql = require("mysql");
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}

const { Sequelize } = require('sequelize');

const db = new Sequelize('cheese', 'cheese_main', 'cheese_main', {
    host: 'cheese.mysql.rds.aliyuncs.com',
    port: "3306",
    dialect: "mysql"
})
var roomModel = db.define('basketroom', {
    basketname: Sequelize.STRING,
    createTime: Sequelize.STRING,
    type: Sequelize.STRING,
    username: Sequelize.STRING,
    phone: Sequelize.STRING,
    userId: Sequelize.STRING
}, { timestamps: false, freezeTableName: true })

var skillModel = db.define('skills', {
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    createTime: Sequelize.STRING
}, { timestamps: false, freezeTableName: true })
const connect = mysql.createConnection(options)
const getbasket = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from baskets limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from baskets"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

const add = (params) => {
    console.log(params, "=======>api");
    let { name, addr, num, introduce, imgurl, open_time, b_tel, useNum, instance } = params
    const add_sql = `insert into baskets(name,addr,num,introduce,imgurl,open_time,b_tel,useNum,instance )   VALUES (?,?,?,?,?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [name, addr, num, introduce, imgurl, open_time, b_tel, useNum, instance], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}

const update = (params) => {
    let { useNum, id } = params
    let data = [useNum, id]
    const update_sql = `update baskets set useNum=? where id=?`
    const p = new Promise((reslove, reject) => {
        connect.query(update_sql, data, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}
const del = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from baskets  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
const queryBasketroom = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await roomModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}

const addbasketroom = async (params) => {
    console.log(params);
    var res = await roomModel.create({
        ...params,
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
const delbasketroom = async (params) => {
    var res = await roomModel.destroy({
        where: {
            ...params
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
// 技巧接口
const getskills=async(params)=>{
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await skillModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const addskills=async(params)=>{
    console.log(params);
    var res = await skillModel.create({
        ...params,
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
const delskills=async(params)=>{
    var res = await skillModel.destroy({
        where: {
            ...params
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
module.exports = {
    getbasket,
    add,
    update,
    del,
    queryBasketroom,
    addbasketroom,
    delbasketroom,
    getskills,
    addskills,
    delskills
}