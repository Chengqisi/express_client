const { param } = require('express/lib/request');
const { Sequelize } = require('sequelize');

const db = new Sequelize('cheese', 'cheese_main', 'cheese_main', {
    host: 'cheese.mysql.rds.aliyuncs.com',
    port: "3306",
    dialect: "mysql"
})
const userModel = db.define('users', {
    score: Sequelize.STRING,
    birth: Sequelize.STRING,
    tel: Sequelize.STRING,
    name: Sequelize.STRING,
    age: Sequelize.INTEGER,
    sex: Sequelize.STRING,
    addr: Sequelize.STRING,
    username: Sequelize.STRING,
    password: Sequelize.STRING
}, { timestamps: false })
const bookModel = db.define('books', {
    img: Sequelize.TEXT,
    name: Sequelize.STRING,
    author: Sequelize.STRING,
    type: Sequelize.STRING,
    msg: Sequelize.STRING
}, { timestamps: false, freezeTableName: true })
//图书评论
const commentModel = db.define('comments', {
    bookname: Sequelize.STRING,
    username: Sequelize.STRING,
    content: Sequelize.STRING,
    time: Sequelize.STRING,
    bookId: Sequelize.STRING
}, { timestamps: false })
// 篮球社区推荐物品
const basketGodds = db.define('goods', {
    imgurl: Sequelize.TEXT,
    name: Sequelize.STRING,
    msg: Sequelize.STRING,
    url: Sequelize.STRING,
    tag: Sequelize.STRING,
    price: Sequelize.STRING
}, { timestamps: false })
// 篮球社区
const homeModel = db.define('communities', {
    name: Sequelize.STRING,
    time: Sequelize.STRING,
    c_msg: Sequelize.STRING,
    comments: Sequelize.STRING,
    like: Sequelize.STRING,
    eyes: Sequelize.STRING
}, { timestamps: false })
// 篮球馆
const roomModel = db.define('baskets', {
    useNum: Sequelize.STRING,
    instance: Sequelize.STRING,
    name: Sequelize.STRING,
    addr: Sequelize.STRING,
    num: Sequelize.STRING,
    imgurl: Sequelize.TEXT,
    open_time: Sequelize.STRING,
    b_tel: Sequelize.STRING,
    introduce: Sequelize.STRING
}, { timestamps: false })
// 新闻资讯
const newsModel = db.define('news', {
    title: Sequelize.STRING,
    comment: Sequelize.STRING,
    createtime: Sequelize.STRING
}, { timestamps: false, freezeTableName: true })
// 医疗物品兑换
const scoreModel = db.define('scoregoods', {
    name: Sequelize.STRING,
    score: Sequelize.STRING,
    introduce: Sequelize.STRING,
    img: Sequelize.TEXT,
    useNum: Sequelize.STRING
}, { timestamps: false })
// 人员录入
const peopleModel = db.define('people', {
    name: Sequelize.STRING,
    age: Sequelize.STRING,
    sex: Sequelize.STRING,
    provice: Sequelize.STRING,
    city: Sequelize.STRING,
    temp: Sequelize.STRING,
    createTime: Sequelize.STRING,
    isHigh: Sequelize.STRING,
    phone: Sequelize.STRING
}, {
    timestamps: false
})
// 用户条件查询
const queryUser = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    var res = await userModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const updateUser = async (params) => {
    const { id, password } = params
    var res = await userModel.update({
        password: password
    }, {
        where: {
            id: id
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data

}
// 图书条件查询
const queryBook = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    var res = await bookModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
// 评论
const queryComment = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await commentModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}

const querbookBasketGoods = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await basketGodds.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
//篮球社区
const queryhome = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await homeModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const addhome = async (params) => {
    console.log(params);
    var res = await homeModel.create({
        ...params,
        like: 0,
        eyes: 0
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
const updatehomelike = async (params) => {
    const { id, like } = params
    var res = await homeModel.update({
        like: parseInt(like) + 1
    }, {
        where: {
            id: id
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const updatehomeeye = async (params) => {
    const { id, eyes } = params
    var res = await homeModel.update({
        eyes: parseInt(eyes) + 1
    }, {
        where: {
            id: id
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
// 
const queryroom = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await roomModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}

const queryNews = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await newsModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const addNews = async (params) => {
    console.log(params);
    var res = await newsModel.create({
        ...params
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}

const delNews = async (params) => {
    var res = await newsModel.destroy({
        where: {
            ...params
        }
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
const queryscore = async (params) => {
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await scoreModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const getpeople = async (params) => {
    console.log(params);
    const page = params.page
    const limit = params.limit
    delete params.page
    delete params.limit
    console.log(params);
    var res = await peopleModel.findAndCountAll({
        where: {
            ...params
        },
        offset: (parseInt(page) - 1) * parseInt(limit),
        limit: parseInt(limit),
    })
    console.log(res);
    let data = {
        ...res,
        code: 0,
        msg: 'success'
    }
    return data
}
const addpeople = async (params) => {
    console.log(params);
    var res = await peopleModel.create({
        ...params
    })
    let data = {
        ...res,
        code: 0,
        msg: "success"
    }
    return data
}
module.exports = {
    queryUser,
    queryBook,
    queryComment,
    querbookBasketGoods,
    queryhome,
    queryroom,
    queryNews,
    addNews,
    delNews,
    queryscore,
    getpeople,
    addpeople,
    updateUser, addhome, updatehomelike,
    updatehomeeye
}