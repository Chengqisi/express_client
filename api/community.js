const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
const getcom = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from communities limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from communities"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

// id       int auto_increment
// primary key,
// name     varchar(20)    null,
// time     varchar(20)    null,
// c_msg    varchar(20)    null,
// comments varchar(10000) null
const add = (params) => {
    console.log(params, "=======>api");
    let { name, time, c_msg, comments } = params
    let like = '0',
        eyes = '0';
    const reg_sql = `insert into communities(name,time,c_msg,comments,like,eyes) VALUES (?,?,?,?,?,?) `
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, [name, time, c_msg, comments, like, eyes], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}


const update = (params) => {
    let { comments, id } = params
    let data = [comments, id]
    const update_sql = `update communities set comments=? where id=?`
    const p = new Promise((reslove, reject) => {
        connect.query(update_sql, data, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}

const del = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from communities  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
module.exports = {
    getcom,
    add,
    update,
    del
}