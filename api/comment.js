const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
const getcomment = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from comments limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from  comments"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}
// create table comment
// (
//     id       int auto_increment
//         primary key,
//     bookname varchar(20)   null,
//     username varchar(20)   null,
//     content  varchar(1000) null,
//     time     varchar(20)   null,
//     bookId   varchar(100)  null
// );

const addcomment = (params) => {
    console.log(params, "=======>api");
    let { bookname, username, content, time, bookId } = params
    const add_sql = `insert into comments(bookname, username, content, time, bookId )   VALUES (?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [bookname, username, content, time, bookId], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}

const delcomment = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from comments  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}

// 查询
const querycomment = (params) => {
    console.log(params, "============.params");
    const { bookId, page, size } = params
    let limit = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    const quer_sql = `select * from comments where bookId ='${bookId}' limit ?,?`
    const p = new Promise((reslove, reject) => {
        connect.query(quer_sql, limit, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                let select_total = `select count(*) as total from comments where bookId='${bookId}'`
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        reslove(reslut)
                    }
                })
            }
        })
    })
    return p
}
module.exports = {
    getcomment,
    addcomment,
    delcomment,
    querycomment
}