const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}


const connect = mysql.createConnection(options)
const getgood = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from scoregoods limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from scoregoods"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}
// name      varchar(100) null,
// score     varchar(20)  null,
// introduce varchar(200) null,
// img       text         null,
// useNum    varchar(20)  null
const add = (params) => {
    console.log(params, "=======>api");
    let { name, score, introduce, img, useNum} = params
    const add_sql = `insert into scoregoods(name, score, introduce, img, useNum)   VALUES (?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [name, score, introduce, img, useNum],function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true ,data:data})
            }
        })
    })
    return p
}

const update = (params) => {
    let { useNum, id } = params
    let data = [useNum, id]
    const update_sql = `update scoregoods set useNum=? where id=?`
    const p = new Promise((reslove, reject) => {
        connect.query(update_sql, data, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}
module.exports = {
    getgood,
    add,
    update
}