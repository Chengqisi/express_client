const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
const getgood = ({ page, size }) => {

    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from books limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from books"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}
// img    text        null,
// name   varchar(20) null,
// author varchar(20) null,
// type   varchar(2)  null
const add = (params) => {
    console.log(params, "=======>api");
    let { name, img, author, type, msg } = params
    const add_sql = `insert into books(name, img,author,type,msg)   VALUES (?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [name, img, author, type, msg], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}

const del = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from books  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
const querybook = (params) => {
    console.log(params, "============.params");
    const { type, page, size } = params
    let limit = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    const quer_sql = `select * from books where type ='${type}' limit ?,?`
    const p = new Promise((reslove, reject) => {
        connect.query(quer_sql, limit, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                let select_total = `select count(*) as total from books where type='${type}'`
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        reslove(reslut)
                    }
                })
            }
        })
    })
    return p
}

const querybookId = (params) => {
    console.log(params, "============.params");
    const { size, page, id } = params
    let limit = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    const quer_sql = `select * from books where id ='${parseInt(id)}' limit ?,?`
    const p = new Promise((reslove, reject) => {
        connect.query(quer_sql, limit, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                let select_total = `select count(*) as total from books where id='${parseInt(id)}'`
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        reslove(reslut)
                    }
                })
            }
        })
    })
    return p
}
// bookId varchar(20) null,
// name   varchar(20) null,
// info   longtext    null
const addchapter = (params) => {
    let { bookId, name, info } = params
    const add_sql = `insert into chatper(bookId, name, info)   VALUES (?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [bookId, name, info], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}

const delchapter = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from chatper  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}

const getchapter = ({ page, size }) => {

    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from chatper limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from chatper"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

const querychapter = (params) => {
    const { bookId, page, size } = params
    let limit = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    const quer_sql = `select * from chatper where bookId =${bookId} limit ?,?`

    const p = new Promise((reslove, reject) => {
        connect.query(quer_sql, limit, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                let select_total = `select count(*) as total from chatper where bookId=${bookId}`

                // const data = JSON.parse(JSON.stringify(res))
                // console.log(data);
                // reslove(data)

                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        reslove(reslut)
                    }
                })
            }
        })
    })
    return p
}

// 推荐图书
const recommendedbook = ({ page, size }) => {
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from bookstore limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from bookstore"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}
// create table bookstore
// (
//     id     int auto_increment
//         primary key,
//     img    text         null,
//     name   varchar(20)  null,
//     author varchar(20)  null,
//     price  varchar(2)   null,
//     url    varchar(100) null
// );

// 新增推荐图书
const addBook = (params) => {
    let { name, author, price, url, img } = params
    const add_sql = `insert into bookstore(name, author, price,url,img)   VALUES (?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(add_sql, [name, author, price, url, img], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}
module.exports = {
    getgood,
    add,
    del,
    querybook,
    addchapter,
    delchapter,
    getchapter,
    querychapter,
    recommendedbook,
    addBook,
    querybookId
}