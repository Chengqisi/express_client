const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
// 获取用户信息
const getuser = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from users limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from users"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}
// 登录
const login = (params) => {
    console.log(params, "=======>api");
    let { username, password } = params
    const login_sql = 'select * from  users where username=? and password=?'
    const p = new Promise((reslove, reject) => {
        connect.query(login_sql, [username, password], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                if (data.length > 0) {
                    const reslut = {
                        sucess: true,
                        code: 0,
                        userinfo:data[0]
                    }
                    reslove(reslut)
                } else {
                    reslove({ code: 404 })
                }

            }
        })
    })
    return p
}

const reg = (params) => {
    let { name,sex,age,tel,addr,birth,username,password } = params,
    score=0;
    const reg_sql =`insert into users(name,sex,age,tel,addr,birth,username,password,score)   VALUES (?,?,?,?,?,?,?,?,?)`
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql,[name,sex,age,tel,addr,birth,username,password,score],function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({code:0,sucess:true})
            

            }
        })
    })
    return p
}

const update = (params) => {
    let { score, id } = params
    let data = [score, parseInt(id)]
    const update_sql = `update users set score=? where id=?`
    const p = new Promise((reslove, reject) => {
        connect.query(update_sql, data, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                reslove({ code: 0, sucess: true, data: data })
            }
        })
    })
    return p
}
module.exports = {
    getuser,
    login,
    reg,
    update
}