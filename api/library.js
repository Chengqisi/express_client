const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
const getlibrary = ({ page, size }) => {
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from library limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from library"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

const dellibrary = (params) => {
    console.log(params, "=======>api");
    let { id } = params
    const reg_sql = 'delete from library  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}

const addlibrary = (params) => {
    console.log(params, "=======>api");
    let { name, distance, msg, img } = params
    const reg_sql = `insert into library(name, distance, msg, img) VALUES (?,?,?,?) `
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, [name, distance, msg, img], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
module.exports = {
    getlibrary,
    dellibrary,
    addlibrary
}