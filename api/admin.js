const mysql = require("mysql")
const options = {
    host: 'cheese.mysql.rds.aliyuncs.com',
    user: 'cheese_main',
    password: 'cheese_main',
    database: 'cheese',
    port: '3306',
}
const connect = mysql.createConnection(options)
const getadmin = ({ page, size }) => {
    // const page = 1,
    //     size = 10;
    let data = []
    let params = [(parseInt(page) - 1) * parseInt(size), parseInt(size)]
    console.log(params);
    const GET_USER = "select * from admin limit ?,?"
    const p = new Promise((resolve, rej) => {
        connect.query(GET_USER, params, function (err, res) {
            if (err) {
                rej(err)
            } else {
                let select_total = "select count(*) as total from admin"
                let res_data = res
                connect.query(select_total, function (t_err, t_res) {
                    if (t_err) { console.log(t_err); }
                    else {
                        console.log(t_res[0]);
                        const total = t_res[0].total
                        const reslut = {
                            total: total,
                            page: parseInt(page),
                            size: parseInt(size),
                            data: JSON.parse(JSON.stringify(res_data)),
                            code: 0,
                            sucess: true
                        }
                        resolve(reslut)
                    }
                })

            }
        })
    })
    return p
}

const login = (params) => {
    console.log(params, "=======>api");
    let { username, password } = params
    const login_sql = 'select * from  admin where username=? and password=?'
    const p = new Promise((reslove, reject) => {
        connect.query(login_sql, [username, password], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                if (data.length > 0) {
                    const reslut = {
                        sucess: true,
                        code: 0
                    }
                    reslove(reslut)
                } else {
                    reslove({ code: 404 })
                }

            }
        })
    })
    return p
}


// 新增

const reg = (params) => {
    console.log(params, "=======>api");
    let { username, password } = params
    const reg_sql = `insert into admin(username,password) VALUES (?,?) `
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, [username, password], function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
// 删除
const del = (params) => {
    console.log(params, "=======>api");
    let { username, password, id } = params
    const reg_sql = 'delete from admin  where id= ?'
    const p = new Promise((reslove, reject) => {
        connect.query(reg_sql, id, function (err, res) {
            if (err) {
                console.log(err);
                reject()
            } else {
                const data = JSON.parse(JSON.stringify(res))
                console.log(data);
                reslove({ code: 0, sucess: true })


            }
        })
    })
    return p
}
module.exports = {
    getadmin,
    login,
    reg,
    del
}