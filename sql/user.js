const insert_user_Sql = `insert into CHEESE(name,sex,age,tel,addr,birth,username,password)   VALUES (?,?,?,?,?,?,?,?)`
const admin_sql= `insert into admin(username,password)   VALUES ? `
const basket_sql= `insert into basket(name,num,addr,b_tel,introduce,open_time)   VALUES ? `
const good_sql=`insert into goods(name,price,url,msg,tag)   VALUES ? `
const com_sql=  `insert into community(name,time,c_msg)   VALUES ? ` 

module.exports={
    insert_user_Sql,
    admin_sql,
    basket_sql,
    good_sql,
    com_sql
}