var express = require('express');
var router = express.Router();
const Api = require("../api/index")

/* GET users listing. */
router.get('/getadmin', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.AdminApi.getadmin(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
// 登录
router.post('/login', async function (req, res, next) {
    console.log(req.query);
    const params={...req.query}
   await Api.AdminApi.login(params).then($data => {
      const data = $data
      console.log(data);
      res.send(data)
    }).catch(err => {
      throw (err)
    })
  });
// 新增
  router.post('/reg', async function (req, res, next) {
    console.log(req.query);
    const params={...req.query}
   await Api.AdminApi.reg(params).then($data => {
      const data = $data
      console.log(data);
      res.send(data)
    }).catch(err => {
      throw (err)
    })
  });

  //删除
  router.post('/del', async function (req, res, next) {
    console.log(req.query);
    const params={...req.query}
   await Api.AdminApi.del(params).then($data => {
      const data = $data
      console.log(data);
      res.send(data)
    }).catch(err => {
      throw (err)
    })
  });
module.exports = router;
