var express = require('express');
var router = express.Router();
const Api = require("../api/index")

/* GET users listing. */
router.get('/', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.UserApi.getuser(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
// 登录
router.post('/login', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.UserApi.login(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

// 新增
router.post('/reg', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.UserApi.reg(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/update', async function (req, res, next) {
  console.log(req.query, req.body);
  const params = { ...req.query }
  await Api.UserApi.update(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.get('/queryusers', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  console.log(params,"========>req.query");
  const data = await Api.testApi.queryUser(params)
  res.send(data)
});

router.get('/updatepassword', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  console.log(params,"========>req.query");
  const data = await Api.testApi.updateUser(params)
  res.send(data)
});
module.exports = router;
