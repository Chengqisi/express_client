var express = require('express');
var router = express.Router();
const Api = require("../api/index")

/* GET users listing. */
router.get('/getcom', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.comApi.getcom(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});


// 新增
router.post('/add', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.testApi.addhome(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

// 更新
router.post('/update', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.comApi.update(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
// 删除

router.post('/del', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.comApi.del(params).then($data => {
    const data = $data
    console.log(data);
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.get('/querycom', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.testApi.queryhome(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/updatelike', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.testApi.updatehomelike(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/updateeye', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.testApi.updatehomeeye(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
module.exports = router;
