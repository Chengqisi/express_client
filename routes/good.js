var express = require('express');
var router = express.Router();
const Api = require("../api/index")

/* GET users listing. */
router.get('/getgood', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.GoodApi.getgood(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.post('/add', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.GoodApi.add(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});


router.get('/querybasketgood', async function (req, res, next) {
    console.log(req.query);
    const params={...req.query}
   await Api.testApi.querbookBasketGoods(params).then($data => {
      const data = $data
      console.log(data);
      res.send(data)
    }).catch(err => {
      throw (err)
    })
  });

router.post('/del', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.GoodApi.del(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
module.exports = router;
