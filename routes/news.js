var express = require('express');
var router = express.Router();
var axios = require('axios')
const Api = require("../api/index")
router.get('/getnews', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    console.log(params, "========>req.query");
    const data = await Api.testApi.queryNews(params)
    res.send(data)
});
router.get('/addnews', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    console.log(params, "========>req.query");
    const data = await Api.testApi.addNews(params)
    res.send(data)
});

router.post('/delnews', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    console.log(params, "========>req.query");
    const data = await Api.testApi.delNews(params)
    res.send(data)
});

module.exports = router;
