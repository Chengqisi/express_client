var express = require('express');
var router = express.Router();
const Api = require("../../api/index")

/* GET users listing. */
router.get('/recommendedbook', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.recommendedbook(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.post('/addbook', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.body }
    await Api.bookApi.addBook(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
module.exports = router;
