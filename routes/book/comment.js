var express = require('express');
var router = express.Router();
const Api = require("../../api/index")

/* GET users listing. */
router.get('/getcomment', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.commentApi.getcomment(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.post('/addcomment', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.body }
    await Api.commentApi.addcomment(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.post('/delcomment', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.commentApi.delcomment(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.get('/querycomment', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.commentApi.querycomment(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.get('/queryCommentV2', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    const data = await Api.testApi.queryComment(params)
    res.send(data)
});
module.exports = router;
