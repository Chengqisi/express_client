var express = require('express');
var router = express.Router();
const Api = require("../../api/index")

/* GET users listing. */
router.get('/getbook', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.getgood(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.post('/add', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.body }
    await Api.bookApi.add(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
//删除
router.post('/del', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.del(params).then($data => {
        const data = $data
        console.log(data);
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
// 查询图书
router.get('/querybook', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    const data = await Api.testApi.queryBook(params)
    res.send(data)
});
// 查询图书Id
router.get('/querybookId', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.querybookId(params).then($data => {
        const data = $data
        console.log(data);
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
//   新增章节
router.post('/addchapter', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.body }
    await Api.bookApi.addchapter(params).then($data => {
        const data = $data
        console.log(data);
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.post('/delchapter', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.delchapter(params).then($data => {
        const data = $data
        console.log(data);
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.get('/getchapter', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.getchapter(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.get('/querychapter', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.bookApi.querychapter(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
module.exports = router;
