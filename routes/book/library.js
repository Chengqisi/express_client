var express = require('express');
var router = express.Router();
const Api = require("../../api/index")
var axios = require('axios');
const { log } = require('debug');

/* GET users listing. */
router.get('/getlibrary', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.librayApi.getlibrary(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.post('/dellibrary', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.librayApi.dellibrary(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.post('/addlibrary', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.body }
    await Api.librayApi.addlibrary(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

// 高德
router.get('/getDitu', function (req, res, next) {
    const { keywords, city, offset, page } = req.query
    console.log(req.query);
    axios({
        method: "GET",
        url: 'https://restapi.amap.com/v3/place/text',
        params: {
            keywords: keywords,
            city: city,
            offset: offset,
            page: page,
            key: "fb76fef683cf85ff693d9625ed05e4af",
            extensions: "all",
            children: 2
        }
    }).then(data => {
        res.send(data.data)
    })

});

router.get('/getDituImg', function (req, res, next) {
    const { entr_location, city } = req.query
    axios({
        method: "GET",
        url: 'https://restapi.amap.com/v3/staticmap',
        params: {
            location: entr_location,
            labels: `${city},2,0,16,0xFFFFFF`,
            size: '200*200',
            key: "fb76fef683cf85ff693d9625ed05e4af",
            zoom:10
        }
    }).then(data => {
        res.send(data.data)
    })

});

module.exports = router;
