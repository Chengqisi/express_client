var express = require('express');
var router = express.Router();
var axios = require('axios')
/* GET home page. */
// 查询新冠数据
router.get('/getdata', function (req, res, next) {
  // res.render('index', { title: 'Express' });
  axios({
    method: "GET",
    url: "https://c.m.163.com/ug/api/wuhan/app/data/list-total"
  }).then(data => {
    res.send(data.data)
  })
});
// 查询接种点
router.get('/gethospital', function (req, res, next) {
  console.log(req.query);
  const params={
    longitude: req.query.longitude,
    latitude: req.query.latitude,
  }
  axios({
    method: "GET",
    url: "https://ditu.amap.com/place/text",
    params:{
      longitude: req.query.longitude,
      latitude: req.query.latitude,
    }
  }).then(data => {
    console.log(data);
    res.send(data.data)
  })
})

router.get('/test',function(req,res,next){
  res.send('成功')
})

module.exports = router;
