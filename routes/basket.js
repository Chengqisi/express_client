var express = require('express');
var router = express.Router();
const Api = require("../api/index")

/* GET users listing. */
router.get('/getbasket', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.queryBasketroom(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});


router.post('/delbasketroom', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.basketApi.delbasketroom(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/addbasketroom', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.body }
  await Api.basketApi.addbasketroom(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.get('/getskill', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.getskills(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
router.post('/addskills', async function (req, res, next) {
  console.log(req.body);
  const params = { ...req.query }
  await Api.basketApi.addskills(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
router.post('/delskill', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.delskills(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/add', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.add(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.post('/update', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.update(params).then($data => {
    const data = $data
    console.log(data, "========>data");
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});


router.post('/del', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.basketApi.del(params).then($data => {
    const data = $data
    console.log(data);
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});

router.get('/queryroom', async function (req, res, next) {
  console.log(req.query);
  const params = { ...req.query }
  await Api.testApi.queryroom(params).then($data => {
    const data = $data
    res.send(data)
  }).catch(err => {
    throw (err)
  })
});
module.exports = router;
