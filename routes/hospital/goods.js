var express = require('express');
var router = express.Router();
const Api = require("../../api/index")

/* GET users listing. */
router.get('/getgood', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.scoreApi.getgood(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.post('/add', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.scoreApi.add(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
router.post('/update', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.scoreApi.update(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.get('/querygood', async function (req, res, next) {
    console.log(req.query);
    const params = { ...req.query }
    await Api.testApi.queryscore(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.get('/getpeople', async function (req, res, next) {
    const params = { ...req.query }
    await Api.testApi.getpeople(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});

router.get('/addpeople', async function (req, res, next) {
    const params = { ...req.query }
    await Api.testApi.addpeople(params).then($data => {
        const data = $data
        res.send(data)
    }).catch(err => {
        throw (err)
    })
});
module.exports = router;
