const Mock = require("mockjs")

// name   varchar(20)    null,
// price  varchar(10)    null,
// msg    varchar(10000) null,
// url    varchar(1600)  null,
// imgurl varchar(1600)  null,
// tag    varchar(10)    null
const good = Mock.mock({
    "user|10": [{
        "name": "@cname",
        "price|5-100": 1,
        "url": "@county(true)",
        "msg":'@cword(100)',
        "tag|1-10":1
    }]
})
const goodList = good.user
goodList.map((item, index) => {
    goodList[index] = [item.name, item.price,item.url,item.msg,item.tag]
})
module.exports= goodList