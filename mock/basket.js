const Mock = require("mockjs")

// name      varchar(20)    null,
// addr      varchar(30)    null,
// num       int            null,
// indroduce varchar(100)   null,
// imgurl    varchar(16000) null,
// open_time varchar(20)    null,
// b_tel     int            null
const basket = Mock.mock({
    "user|10": [{
        "name": "@cname",
        "num|5-100": 1,
        "addr": "@county(true)",
        "b_tel": /^1[34578]\d{9}$/,
        "introduce":'@cword(100)',
        "open_time":"@date()"
    }]
})
const basketList = basket.user
basketList.map((item, index) => {
    basketList[index] = [item.name, item.num,item.addr,item.b_tel,item.introduce,item.open_time]
})
module.exports= basketList