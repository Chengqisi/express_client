const Mock = require("mockjs")

const user = Mock.mock({
    "user|10": [{
        "name": "@cname",
        "age|20-25": 1,
        "sex": "@Boolean"?'男':'女',
        "addr": "@county(true)",
        "tel": /^1[34578]\d{9}$/,
        "birth":"@date()"
    }]
})
const userList = user.user
userList.map((item, index) => {
    userList[index] = [item.name, item.sex, item.age, item.tel, item.addr,item.birth]
})
module.exports= userList